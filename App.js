import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, compose } from 'redux';
import reducers from './src/reducers/index';
import { createStackNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import subAppPage from './src/subApp/subAppPage';
import CheckList from './src/checklist/Checklist';

type Props = {};
let composeEnhancers = compose;
if (__DEV__) {
  composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}
const store = createStore(
  reducers,
  {},
  composeEnhancers()
);

const stackNavigator = createSwitchNavigator({
  checkList: CheckList,
  subApp: subAppPage
},
  {
    initialRouteName: 'checkList'
  }
)

const AppContainer = createAppContainer(stackNavigator)

export default class App extends Component<Props> {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

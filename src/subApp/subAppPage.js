import React, { Component } from 'react';
import { View, Text } from 'react-native'
import { createStore, compose } from 'redux';
import { Provider } from 'react-redux';
import subAppReducers from './subApp_reducer';
import { Button } from 'react-native-elements';
// if (__DEV__ ) {
//     composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// }

// store = createStore(
//     subAppReducers,
//     {},
//     composeEnhancers()
// );
class subAppPage extends Component {
    constructor(props) {
        super(props);
        if (__DEV__) {
            composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
        }

        this.store = createStore(
            subAppReducers,
            {},
            composeEnhancers()
        );
    }

    render() {
        return (
            <Provider store={this.store}>
                <View >
                    <Text>sub App Page</Text>
                    <Text>sub App Page</Text>
                    <Text>sub App Page</Text>
                    <Text>sub App Page</Text>
                    <Text>sub App Page</Text>
                    <Button
                        title='switch to checklist'
                        onPress={() => { this.props.navigation.navigate('checkList') }}
                    />
                </View>
            </Provider>
        )
    }
}

export default subAppPage;